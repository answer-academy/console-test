﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}


// class Program
// {
//     public static Journey _journey = new Journey();
//     public static Garage _garage = new Garage();
//     public static void Main(string[] args)
//     {
//         System.Console.WriteLine($"Application started at: {DateTime.Now}");

//         _journey.Begin(_garage);
//         _journey.End();

//         System.Console.WriteLine($"Application closed at: {DateTime.Now}");
//         _journey.ExitOrRestart();
//     }
// }