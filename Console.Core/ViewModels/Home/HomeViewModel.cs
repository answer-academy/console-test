﻿using Console.Infra;
using System.Collections.Generic;

namespace ViewModels.Home
{
    public class HomeViewModel
    {
        public int GarageCount { get; set; }
        public Dictionary<int, IVehicle> Garage { get; set; }
    }
}
