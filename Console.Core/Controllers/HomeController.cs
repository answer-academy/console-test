﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using driving.adventure.Models;
using Console.Infra.Interfaces;
using ViewModels.Home;
using Console.Infra;

namespace driving.adventure.Controllers
{
    public class HomeController : Controller
    {
        private IGarage _garage;

        public HomeController(IGarage garage)
        {
            _garage = garage;
        }
        public IActionResult Index()
        {
            HomeViewModel model = new HomeViewModel
            {
                Garage = _garage.GetAll(),
                GarageCount = _garage.GetCount()
            };
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        
        public JsonResult GetJson(int id)
        {
            return Json(_garage.Get(id).Name);
        }

        public string Beep(int id)
        {
            var vehicle = _garage.Get(id);
            return vehicle.Beep();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
