using System.Collections.Generic;
using Console.Infra;
using Console.Infra.Interfaces;

namespace Console.Core
{
    public class Garage : IGarage
    {
        public Dictionary<int, IVehicle> Vehicles { get; private set; }

        public Garage()
        {
            Vehicles = new Dictionary<int, IVehicle>()
            {
                {
                    1,
                    new OneBike ()
                    {
                        Fuel = 150.5F,
                        DistanceTravelled = 2500.25F
                    }
                },
                {
                    2,
                    new MX5()
                    {
                        Fuel = 100F,
                        DistanceTravelled = 10000.5F,
                        DoorsClosed = false
                    }
                },
                {
                    3,
                    new Skyline(10F, 10F)
                },
                {
                    4,    
                    new Skyline(100F, 100F, false)
                },
                {
                    5,
                    new TwoBike()
                    {
                        Fuel = 150.5F,
                        DistanceTravelled = 2500F
                    }
                }
            };
        }

        public IVehicle Get(int vehicleId)
        {
            Vehicles.TryGetValue(vehicleId, out IVehicle vehicle);
            return vehicle;
        }

        public Dictionary<int, IVehicle> GetAll()
        {
            return Vehicles;
        }

        public int GetCount()
        {
            return Vehicles.Count;
        }
    }
}