using System;
using Console.Infra;
using Console.Infra.Interfaces;

namespace Console.Core
{
    public class Journey : IJourney
    {
        public enum StatusEnum
        {
            Started,
            Ended
        }
        public enum EndActions
        {
            exit,
            restart
        }
        public int Status { get; set; }
        public void Begin(IGarage _garage)
        {
            System.Console.WriteLine("Your journey has begun. Please keep arms and legs inside the carriage at all times.");
            Status = (int)StatusEnum.Started;
            var entireGarage = _garage.GetAll();
            System.Console.WriteLine($"Here is your garage:");
            foreach (var myVehicle in entireGarage)
            {
                System.Console.WriteLine($"({myVehicle.Key}) {myVehicle.Value.Name}: Fuel = {myVehicle.Value.Fuel}l");
            }

        }

        public void End()
        {
            System.Console.WriteLine("Your journey has come to an end. We hope you enjoyed your time.");
            Status = (int)StatusEnum.Ended;

        }

        public void ExitOrRestart()
        {
            System.Console.WriteLine("Do you wish to (exit) or (restart)?");
            string nextAction = System.Console.ReadLine().ToString().ToLower();

            if (nextAction == EndActions.exit.ToString())
            {
                Environment.Exit(0);
            }
            else if (nextAction == EndActions.restart.ToString())
            {
                Program.Main(new string[0]);
            }
            else
            {
                System.Console.WriteLine($"Response not recognised. Please try again.");
                ExitOrRestart();
            }
        }

    }
}