namespace Console.Core
{
    public class Skyline : Car
    {
        public Skyline() : base() {}
        public Skyline(float fuel, float distanceTravelled, bool doorsClosed = true) : base (fuel, distanceTravelled, doorsClosed) {}
        private readonly string name = "Nissan Skyline";
        public override string Name
        {
            get { return name; }
        }        
        private float fuel;
        public override float Fuel
        {
            get { return fuel;}
            set
            {
                fuel = value;
            }
        }

        public override void Drive()
        {
            if (DoorsClosed)
            {
                System.Console.WriteLine("SKYLINE GO VROOM");
            }
            else
            {
                System.Console.WriteLine("SKYLINE GO VROOM WITH OPEN DOORS WOOSH");
            }
        }
    }
}