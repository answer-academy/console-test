namespace Console.Core
{
    public class MX5 : Car
    {
        public MX5() : base() { }
        public MX5(float fuel, float distanceTravelled, bool doorsClosed = true) : base(fuel, distanceTravelled, doorsClosed) { }
        public override string Name => "Mazda MX-5";
        public override string Beep()
        {
            return "Car Beep";
        }
    }
}