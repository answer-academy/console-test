using Console.Infra;

namespace Console.Core
{
    public class Car : IVehicle
    {
        public Car() : this(0F, 0F) { }

        public Car(float fuel, float distanceTravelled, bool doorsClosed = true)
        {
            Fuel = fuel;
            DistanceTravelled = distanceTravelled;
            DoorsClosed = doorsClosed;
        }
        public virtual long Id { get; set; }
        public virtual string Name { get; }
        public virtual float Fuel { get; set; }
        private float distanceTravelled;
        public float DistanceTravelled
        {
            get { return distanceTravelled; }
            set
            {
                distanceTravelled = (value > 0) ? value : 0;
            }
        }
        public bool DoorsClosed { get; set; } = true;

        public virtual string Beep()
        {
            return "Car go beep!";
        }

        public virtual void Drive()
        {
            if (DoorsClosed)
            {
                System.Console.WriteLine("CAR GO VROOM");
            }
            else
            {
                System.Console.WriteLine("Closing doors first...");
                CloseDoors();
                System.Console.WriteLine("LATE BUT OFF WE GO~");
            }
        }

        public void OpenDoors()
        {
            System.Console.WriteLine("Doors open!");
            DoorsClosed = false;
        }

        public virtual void CloseDoors()
        {
            System.Console.WriteLine("Doors closed!");
            DoorsClosed = true;
        }
    }
}