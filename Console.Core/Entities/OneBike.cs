namespace Console.Core
{
    public class OneBike : Motorbike
    {
        private readonly string name = "bike 1";
        public override string Name
        {
            get { return name; }
        }
        public override float Fuel { get; set; }

        public override float DistanceTravelled { get; set; }

        public override void Drive()
        {
            System.Console.WriteLine("vroom :]");
        }
    }
}