using Console.Infra;

namespace Console.Core
{
    public abstract class Motorbike : IVehicle
    {
        public virtual long Id { get; set; }
        public abstract string Name { get; }
        public abstract float Fuel { get; set; }
        public abstract float DistanceTravelled { get; set; }

        public virtual string Beep()
        {
            return "Bike go beep!";
        }

        public abstract void Drive();
    }
}