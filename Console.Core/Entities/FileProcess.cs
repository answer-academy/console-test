using System;
using System.IO;

namespace Console.Core
{
    public class FileProcess
    {
        public bool FileExists(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                throw new ArgumentNullException("fileName");
            };
            return File.Exists(fileName);
        }
    }
}