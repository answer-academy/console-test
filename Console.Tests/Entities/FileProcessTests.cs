using System;
using Console.Core;
using Microsoft.Extensions.Configuration;
using Xunit;
using System.IO;

namespace Console.Tests
{
    public class FileProcessTests
    {
        private FileProcess _fileProcess = new FileProcess();
        private IConfiguration _config = TestConfig.InitConfiguration();
        public string GOOD_FILE_NAME { get; set; }
        private const string BAD_FILE_NAME = @"C:\BAD-FILE.txt";
        private void SetFileName()
        {
            GOOD_FILE_NAME = _config["fileName"];
            if (GOOD_FILE_NAME.Contains("[TestingPath]"))
            {
                GOOD_FILE_NAME = GOOD_FILE_NAME.Replace("[TestingPath]",
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            }
        }

        private void CreateFile()
        {
            File.WriteAllText(GOOD_FILE_NAME, "Some sample text");
        }

        private void DeleteFile()
        {
            File.Delete(GOOD_FILE_NAME);
        }

        [Fact]
        public void FileNameDoesExist()
        {
            // Arrange
            SetFileName();
            CreateFile();

            // Act
            bool fileExists = _fileProcess.FileExists(GOOD_FILE_NAME);
            DeleteFile();

            // Assert
            Assert.True(fileExists, $"'{GOOD_FILE_NAME}' does not exist.");
        }

        [Fact]
        public void FileNameDoesNotExist()
        {
            // Arrange
            bool fileExists;

            // Act
            fileExists = _fileProcess.FileExists(BAD_FILE_NAME);

            // Assert
            Assert.False(fileExists, $"'{GOOD_FILE_NAME}' does exist.");
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData("  ")]
        public void FileNameNullOrWhiteSpace_ThrowsArgumentNullException(string fileName)
        {
            // Assert
            Assert.Throws<ArgumentNullException>(() => _fileProcess.FileExists(fileName));
        }

    }
}
