using System.Collections.Generic;
using Console.Core;
using Console.Infra;
using Console.Infra.Interfaces;
using Xunit;

namespace Console.Tests.Entities
{
    public class GarageTests
    {
        [Fact]
        public void ShouldGetFullGarage()
        {
            // Arrange
            var garage = new Garage();
            // Act
            var fullGarage = garage.GetAll();
            // Assert
            Assert.NotEmpty(fullGarage);
        }
        [Fact]
        public void ShouldGetCorrectVehicle()
        {
            // Arrange
            var garage = new Garage();
            int vehicleId = 21;
            // Act
            var singleVehicle = garage.Get(vehicleId);
            // Assert
            Assert.Equal(singleVehicle.Id, vehicleId);
        }
    }
}