using System.IO;
using Console.Core;
using Console.Infra;
using Console.Infra.Interfaces;
using Xunit;

namespace Console.Tests
{
    public class JourneyTests
    {
        public static Journey _journey = new Journey();
        public IGarage _garage = new Garage();

        [Fact]
        public void JourneyStatusIsStarted()
        {
            // Arrange
            _journey.Status = (int)Journey.StatusEnum.Ended;

            // Act
            _journey.Begin(_garage);

            // Assert
            Assert.Equal(_journey.Status, (int)Journey.StatusEnum.Started);
        }

        [Fact]
        public void JourneyStatusIsEnded()
        {
            // Arrange
            _journey.Status = (int)Journey.StatusEnum.Started;

            // Act
            _journey.End();

            // Assert
            Assert.Equal(_journey.Status, (int)Journey.StatusEnum.Ended);
        }
    }
}