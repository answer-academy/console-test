using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Collections.Generic;
using Console.Core;
using Console.Infra;

namespace Console.Tests
{
    [TestClass]
    public class MX5Test
    {
        private IConfiguration Config { get; set; } = TestConfig.InitConfiguration();
        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
            testContext.WriteLine("MX5Test initialisation started.");
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {

        }

        [TestInitialize]
        public void TestInitialize()
        {
            TestContext.WriteLine($"{TestContext.TestName} test initialisation started in {Directory.GetCurrentDirectory()}.");
        }

        [TestCleanup]
        public void TestCleanup()
        {

        }

        [TestMethod]
        [Description("Test to check that a new MX5 object is able to return the expected name string variable.")]
        [Owner("Me!")]
        [Priority(0)]
        [TestCategory("Good Tests")]
        //[Ignore]
        [Timeout(2000)]
        public void MX5HasExpectedName()
        {
            var mx5 = new MX5();
            var expected = Config.GetSection("AppSettings")["MX5Name"];
            TestContext.WriteLine($"Retrieved MAX5Name '{expected}' from config file.");

            var actual = mx5.Name;

            Assert.AreEqual(expected, actual, $"Expected: '{expected}' Actual: '{actual}'.");
        }

        [TestMethod]
        public void MX5FuelIsGettable()
        {
            var mx5 = new MX5(100.5F, 200.5F);
            var expected = 100.5F;

            var actual = mx5.Fuel;

            Assert.AreEqual(expected, actual, $"Expected: '{expected}' Actual: '{actual}'.");
        }

        [TestMethod]
        public void MX5DistanceTravelledIsGettable()
        {
            var mx5 = new MX5(100.5F, 200.5F);
            var expected = 200.5F;

            var actual = mx5.DistanceTravelled;

            Assert.AreEqual(expected, actual, $"Expected: '{expected}' Actual: '{actual}'.");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MX5FuelLessThanZero_ThrowsArgumentOutOfRangeException()
        {
            var mx5 = new MX5();
            mx5.Fuel = -1.0F;
            Assert.Inconclusive();
        }

        [TestMethod]
        public void DummyTestForCollectionsEqual()
        {
            List<IVehicle> vehicles1 = new List<IVehicle>()
            {
                new MX5(1F, 1F),
                new MX5(2F, 2F)
            };

            List<IVehicle> vehicles2 = new List<IVehicle>()
            {
                new MX5(1F, 1F),
                new MX5(2F, 2F)
            };

            CollectionAssert.AreEqual(vehicles1, vehicles2,
                Comparer<IVehicle>.Create((x, y) =>
                    (x.Name == y.Name && x.Fuel == y.Fuel && x.DistanceTravelled == y.DistanceTravelled) ? 0 : 1)
            );
        }

        [TestMethod]
        public void DummyTestForCollectionsNotEqual()
        {
            List<IVehicle> vehicles1 = new List<IVehicle>()
            {
                new MX5(1F, 1F),
                new MX5(2F, 2F)
            };

            List<IVehicle> vehicles2 = new List<IVehicle>()
            {
                new MX5(1F, 1F),
                new MX5(2F, 2F)
            };

            CollectionAssert.AreNotEqual(vehicles1, vehicles2);
        }

        [TestMethod]
        public void DummyTestForCollectionsAllMX5s()
        {
            List<IVehicle> vehicles = new List<IVehicle>()
            {
                new MX5(1F, 1F),
                new MX5(2F, 2F)
            };

            CollectionAssert.AllItemsAreInstancesOfType(vehicles, typeof(MX5));
        }
    }
}
