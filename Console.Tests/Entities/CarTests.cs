using Console.Core;
using Xunit;

namespace Console.Tests
{
    public class CarTests
    {
        private Car _car = new Car();

        [Fact]
        public void Should_CloseDoors()
        {
            // Arrange
            _car.DoorsClosed = false;

            // Act
            _car.CloseDoors();

            // Assert
            Assert.True(_car.DoorsClosed);
        }

        [Fact]
        public void Should_CloseDoors_When_Driving()
        {
            // Arrange
            _car.DoorsClosed = false;

            // Act
            _car.Drive();

            // Assert
            Assert.True(_car.DoorsClosed);
        }
    }
}
