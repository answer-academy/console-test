using Microsoft.Extensions.Configuration;
using System;

namespace Console.Tests
{
    public static class TestConfig
    {
        public static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.test.json")
                .Build();
            return config;
        }
    }
}