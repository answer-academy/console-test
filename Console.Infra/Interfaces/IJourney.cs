
using Console.Infra.Interfaces;

namespace Console.Infra
{
    public interface IJourney
    {
        int Status { get; set; }
        void Begin(IGarage _garage);
        void End();
    }
}