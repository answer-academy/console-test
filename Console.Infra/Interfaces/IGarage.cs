using System.Collections.Generic;

namespace Console.Infra.Interfaces
{
    public interface IGarage
    {
        Dictionary<int, IVehicle> GetAll();
        IVehicle Get(int vehicleId);
        int GetCount();
    }
}