namespace Console.Infra
{
    public interface IVehicle
    {
        long Id { get; set; }
        string Name { get; }
        float Fuel { get; set; }
        float DistanceTravelled { get; set; }

        string Beep();
        void Drive();
    }
}